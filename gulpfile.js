var marked = require('./node_modules/marked/lib/marked.js');

var gulp = require('gulp'),
    data = require('gulp-data'),
    path = require('path'),
    fs = require('fs'),
    yaml = require('js-yaml'),
    less = require('gulp-less'),
    twig = require('gulp-twig'),
    livereload = require('gulp-livereload');

gulp.task('less', function(){
    'use strict';
    return gulp.src('src/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('src/templates'))
        .pipe(livereload());
});

gulp.task('js', function(){
    'use strict';
    return gulp.src('src/js/*.js')
        //TODO js post-processing
        .pipe(gulp.dest('src/templates'))
        .pipe(livereload());
});
 
gulp.task('compile', function () {
    'use strict';
    return gulp.src('./src/templates/pages/*.twig')
        .pipe(twig({
            // data: {               
            //     benefits: [
            //         'Fast',
            //         'Flexible',
            //         'Secure'
            //     ]
            // }
            filters: [
                
            ],
            functions: [
               
            ]
        }))
        .pipe(gulp.dest('build'))
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(['data/**/*.yaml', 'src/less/*.less', 'src/js/*.js', 'src/templates/*.twig'], gulp.series('less','js','compile'));
});
 
gulp.task('default', gulp.series('less','js','compile'));