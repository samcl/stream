'use strict';

// listen for changes to document.readyState - onreadystatechange is
// fired when readyState value is changed
document.onreadystatechange = function () {

    // check the value - if it's 'interactive' then the DOM has loaded
    if (document.readyState === "interactive") {

        var text = document.querySelectorAll('.center-container')[0];
        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('text')) {
            console.log('resetting text');
            text.innerText = urlParams.get('text');
        }

        console.log(text);
        var chars = text.innerText.split('');
        text.innerText = '';
        var newText = '';
        var it = 0;
        chars.forEach(function(char){
            var cl = (it % 2 == 0) ? 'even' : 'odd';
            newText+="<span class='z-"+cl+"'>"+char+"</span>";
            it++;
        });
        text.innerHTML = newText;
    }
}